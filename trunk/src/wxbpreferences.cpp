/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "wxbpreferences.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif



WxbPreferences::WxbPreferences (wxWindow* parent, const wxString& title) : wxPropertySheetDialog (parent, -1, title, wxDefaultPosition, wxSize (250,230))
{
	CreateButtons (wxOK | wxCANCEL | wxHELP);
	Connect(wxID_HELP, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(WxbPreferences::onDisplayHelp));
	GetBookCtrl()->AddPage (CreateGeneralPage (), _("General"));

	LayoutDialog ();
}

WxbPreferences::~WxbPreferences ()
{
}

void WxbPreferences::SetSettings (struct s_options f_options)
{
	tc_startpage->SetValue (f_options.startpage);
	checkbox1->SetValue (f_options.savesize);
	spinner21->SetValue (f_options.width);
	spinner22->SetValue (f_options.height);
}

struct s_options WxbPreferences::GetSettings (void)
{
	struct s_options f_options;
	f_options.startpage = tc_startpage->GetValue ();
	f_options.savesize = checkbox1->GetValue ();
	f_options.width = spinner21->GetValue ();
	f_options.height = spinner22->GetValue ();
	return f_options;
}

void WxbPreferences::onDisplayHelp (wxCommandEvent& event)
{
	wxLaunchDefaultBrowser (("file://" DATADIR "/help.html"));
}

wxPanel* WxbPreferences::CreateGeneralPage (void)
{
	wxPanel* panel = new wxPanel (GetBookCtrl (), wxID_ANY);
	wxBoxSizer *vbox = new wxBoxSizer  (wxVERTICAL);
	panel->SetSizer (vbox);

	/* first row */
	wxBoxSizer* hbox11 = new wxBoxSizer (wxHORIZONTAL);
	wxStaticText *text1 = new wxStaticText (panel, wxID_ANY, _("Start page"));
	tc_startpage = new wxTextCtrl (panel, wxbID_STARTPAGE, wxT (""));
	hbox11->Add (text1, 0, wxALIGN_CENTRE|wxALL, 2);
	hbox11->Add (tc_startpage, 1, wxEXPAND|wxALL, 2);

	/* second row */
	wxStaticBoxSizer* staticbox1 = new wxStaticBoxSizer (wxVERTICAL, panel, _("Main window"));

	wxBoxSizer* hbox21 = new wxBoxSizer (wxHORIZONTAL);
	checkbox1 = new wxCheckBox (panel, wxbID_SIZEABLE, _("Set main window size"));
	hbox21->Add (checkbox1, 1, wxEXPAND|wxALL, 2);

	wxBoxSizer* hbox22 = new wxBoxSizer (wxHORIZONTAL);
	text21 = new wxStaticText (panel, wxID_ANY, _("Width"));
	spinner21 = new wxSpinCtrl (panel, wxbID_WIDTH);
	spinner21->SetRange (800,4096);
	text22 = new wxStaticText (panel, wxID_ANY, _("Height"));
	spinner22 = new wxSpinCtrl (panel, wxbID_HEIGHT);
	spinner22->SetRange (600,4096);
	hbox22->Add (text21, 0, wxALIGN_CENTRE|wxALL, 2);
	hbox22->Add (spinner21, 2, wxEXPAND|wxALL, 2);
	hbox22->Add (text22, 0, wxALIGN_CENTRE|wxALL, 2);
	hbox22->Add (spinner22, 2, wxEXPAND|wxALL, 2);
	
	staticbox1->Add (hbox21, 0, wxEXPAND|wxALL, 2);
	staticbox1->Add (hbox22, 0, wxEXPAND|wxALL, 2);

	/* put all together */
	vbox->Add (hbox11, 0, wxEXPAND|wxALL, 2);
	vbox->Add (staticbox1, 0, wxEXPAND|wxALL, 2);

	/* Callbacks */
	Connect (wxbID_SIZEABLE, wxEVT_CHECKBOX, wxCommandEventHandler (WxbPreferences::OnCheckBoxClicked));
	return panel;
}

void WxbPreferences::OnCheckBoxClicked (wxCommandEvent& event)
{
	text21->Enable (event.IsChecked ());
	text22->Enable (event.IsChecked ());
	spinner21->Enable (event.IsChecked ());
	spinner22->Enable (event.IsChecked ());
}
