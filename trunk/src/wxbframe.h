/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __WXBFRAME__
#define __WXBFRAME__

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <wx/wx.h>

#include "wxbtabbook.h"
#include "wxboptions.h"

enum
{
	wxB_TABBOOK = 1001,
	wxB_BOOKMARKS
};

class WxbFrame : public wxFrame
{
	public:
		WxbFrame (wxLocale& locale, const wxString& title, const wxPoint& pos, const wxSize& size);
		~WxbFrame ();
		
	private:
		/* Callback functions */
		void OnQuit (wxCommandEvent& event);
		void OnNewTab (wxCommandEvent& event);
		void OnCloseTab (wxCommandEvent& event);
		void OnPopupClick (wxCommandEvent &event);
		void OnBookmarks (wxCommandEvent& event);
		void OnPreferences (wxCommandEvent& event);
		void OnAbout (wxCommandEvent& event);
		void OnSetMainTitle (wxCommandEvent& event);
		void OnKeyDown (wxKeyEvent& event);
		wxLocale& m_locale;
		WxbTabbook *tabbook;
		WxbOptions *m_Options;
};

#endif
