/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __WXBOPTIONS__
#define __WXBOPTIONS__

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <wx/wx.h>
#include <wx/wfstream.h>
#include <wx/fileconf.h>
#include <wx/dir.h>
#include <string>

struct s_options
{
	wxString startpage;
	bool savesize;
	int height;
	int width;
};

class WxbOptions
{
	public:
		WxbOptions (void);
		~WxbOptions ();
		struct s_options GetSettings (void) const;
		void SetSettings (struct s_options f_options);
		int FetchIntSetting (const char* key);
		wxString FetchStringSetting (const char* key);
		std::string FetchStdStringSetting (const char* key);
	private:
		struct s_options m_options;
		wxFileConfig* fconfig;
};

#endif
