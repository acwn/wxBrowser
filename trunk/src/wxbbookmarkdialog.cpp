/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "wxbbookmarkdialog.h"
#include "wxbbookmarks.h"


WxbBookmarkDialog::WxbBookmarkDialog (wxWindow* parent, const wxString& title) : wxDialog (parent, -1, title, wxDefaultPosition, wxSize (250,230), wxRESIZE_BORDER)
{
	marks = new WxbBookmarks();
	wxBoxSizer* hbox = new wxBoxSizer (wxVERTICAL);

	wxFlexGridSizer* fgs = new wxFlexGridSizer (4, 2, 7, 20);

	fgs->Add (new wxStaticText (this, wxID_ANY, _("Name")));
	m_name = new wxTextCtrl (this, wxB_NAME);
	fgs->Add (m_name, 1, wxEXPAND);

	fgs->Add (new wxStaticText (this, wxID_ANY, _("Address")));
	m_address = new wxTextCtrl (this, wxB_ADDRESS);
	fgs->Add (m_address, 1, wxEXPAND);

	fgs->Add (new wxStaticText (this, wxID_ANY, _("Category")));

	m_category = new wxComboBox (this, wxB_CATEGORY, wxEmptyString, wxDefaultPosition, wxDefaultSize, marks->GetAllCategories (), wxCB_READONLY);
	m_category->SetSelection (0);
	fgs->Add (m_category, 1, wxEXPAND);

	fgs->Add (new wxStaticText (this, wxID_ANY, _("Description")));
	m_description = new wxTextCtrl (this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);
	fgs->Add (m_description, 1, wxEXPAND);

	fgs->AddGrowableCol (1,1);
	fgs->AddGrowableRow (3,1);

	hbox->Add (fgs, 1, wxEXPAND|wxALL,15);
	hbox->Add (CreateStdDialogButtonSizer (wxOK | wxCANCEL | wxHELP));

	SetSizerAndFit (hbox);
	Layout ();

	/* Callbacks */
	Connect (wxB_CATEGORY, wxEVT_TEXT_ENTER, (wxObjectEventFunction) &WxbBookmarkDialog::OnComboboxEnter);
}

WxbBookmarkDialog::~WxbBookmarkDialog ()
{
	delete marks;
}

void WxbBookmarkDialog::OnComboboxEnter (wxCommandEvent& event)
{
	if (m_category->FindString (event.GetString ()) == wxNOT_FOUND)
	{
		m_category->Append (event.GetString ());
	}
}

void WxbBookmarkDialog::SetBookmark (wxString name, wxString address, wxString description)
{
	m_name->SetValue (name);
	m_address->SetValue (address);
	m_description->SetValue (description);
}

int WxbBookmarkDialog::SaveBookmark (void)
{
	try
	{
		WxbBookmarks *marks = new WxbBookmarks();
		marks->SaveFavorite (m_name->GetValue (), m_address->GetValue (), m_category->GetValue (), m_description->GetValue ());
		delete marks;
	}
	catch (std::exception &e)
	{
		wxLogMessage (wxString (e.what (), wxConvUTF8), _("Exception caught"));
	}
}
