/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <wx/wx.h>

#include "wxbframe.h"

class wxBrowser: public wxApp
{
	public:
		virtual bool OnInit();
	protected:
		wxLocale m_locale;
};

bool wxBrowser::OnInit()
{
	try
	{
		/* Initialize the catalogs we'll be using */
		// m_locale.Init(wxLANGUAGE_DEFAULT, wxLOCALE_CONV_ENCODING);
		m_locale.Init ();
		wxLocale::AddCatalogLookupPathPrefix(wxT(LOCALEDIR));
		m_locale.AddCatalog(PACKAGE_NAME);
		WxbFrame* frame = new WxbFrame (m_locale, PACKAGE_NAME, wxDefaultPosition, wxDefaultSize);
		frame->Show(true);
		SetTopWindow(frame);
	}
	catch (std::exception &e)
	{
		wxLogMessage (wxString (e.what (), wxConvUTF8), _("Exception caught"));
	}
	return true;
}

IMPLEMENT_APP(wxBrowser)
