/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "wxbtabbook.h"
#include "wxbtabitem.h"
#include "wxbevents.h"

#include <iostream>
#include <wx/panel.h>

wxDEFINE_EVENT(WXBEVT_NEWTAB, wxCommandEvent);
wxDEFINE_EVENT(WXBEVT_SETMAINTITLE, wxCommandEvent);

WxbTabbook::WxbTabbook (wxWindow *parent, wxWindowID id, wxString startpage) : wxNotebook (parent, id, wxDefaultPosition, wxDefaultSize, 0, wxEmptyString), m_startpage (startpage)
{
	Connect (id, wxEVT_NOTEBOOK_PAGE_CHANGED, (wxObjectEventFunction) &WxbTabbook::ChangedTabTitle);
	Connect (id, wxEVT_NOTEBOOK_PAGE_CHANGING, (wxObjectEventFunction) &WxbTabbook::ChangingTabTitle);
	Connect (id, WXBEVT_NEWTAB, (wxObjectEventFunction) &WxbTabbook::AddNewTab);
	this->AddTab (m_startpage);
}

void WxbTabbook::AddTab (wxString url)
{
	WxbTabitem* panel = new WxbTabitem (this, url, m_startpage);
	this->AddPage (panel, url, true);
	this->Layout ();
}

void WxbTabbook::CloseTab (void)
{
	if (this->GetSelection () != wxNOT_FOUND)
	{
		this->DeletePage (this->GetSelection ());
	}
	if (this->GetPageCount () == 0)
	{
		this->AddTab (m_startpage);
	}
}

void WxbTabbook::ChangedTabTitle (wxBookCtrlEvent& event)
{
	wxString f_text (event.GetString());

	if (!event.GetString().IsEmpty())
	{
		if (event.GetString().Length() >= 20)
		{
			f_text = event.GetString().SubString (0,20);
			f_text.Append ("...");
		}
		SetPageText (GetSelection (), f_text);
		wxCommandEvent f_event (WXBEVT_SETMAINTITLE, GetId());
		f_event.SetEventObject (this);
		f_event.SetString (event.GetString ());
		ProcessWindowEvent (f_event);
	}
}

void WxbTabbook::ChangingTabTitle (wxBookCtrlEvent& event)
{
}

void WxbTabbook::UpdateStartpage (wxString url)
{
	size_t count = GetPageCount ();
	wxWindow *panel;
	
	for (int i=0; i < count; ++i)
	{
		panel = GetPage (i);
	}
}

void WxbTabbook::AddNewTab (wxCommandEvent& event)
{
	AddTab (event.GetString ());
}
