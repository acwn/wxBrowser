/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "wxbmanagebookmarkdialog.h"
#include "wxbchangecategory.h"
#include <string>
#include <wx/splitter.h>
#include <wx/imaglist.h>
#include <wx/artprov.h>

WxbManageBookmarkDialog::WxbManageBookmarkDialog (wxWindow* parent, const wxString& title) : wxDialog (parent, -1, title, wxDefaultPosition, wxSize (250,230), wxRESIZE_BORDER)
{
	marks = new WxbBookmarks ();
	wxBoxSizer* hbox = new wxBoxSizer (wxVERTICAL);

	wxSplitterWindow* splitter = new wxSplitterWindow (this);
	splitter->SplitVertically (CreateLeftPanel (splitter), CreateRightPanel (splitter));
	splitter->SetSashGravity(0.5);
	splitter->SetMinimumPaneSize(150);
	hbox->Add (splitter, 1, wxEXPAND|wxALL, 5);
	hbox->Add (CreateStdDialogButtonSizer (wxOK | wxCANCEL | wxHELP));
	SetSizerAndFit (hbox);
	Layout ();

}

WxbManageBookmarkDialog::~WxbManageBookmarkDialog ()
{
	delete marks;
}

wxPanel* WxbManageBookmarkDialog::CreateLeftPanel (wxWindow* parent)
{
	wxPanel* f_panel = new wxPanel (parent, wxID_ANY);
	wxBoxSizer* f_vbox1 = new wxBoxSizer (wxVERTICAL);

	/* first row */
	wxBoxSizer* f_hbox1 = new wxBoxSizer (wxHORIZONTAL);

	m_categoryadd_button = new wxBitmapButton (f_panel, wxB_CATEGORY_ADD, wxArtProvider::GetBitmap (wxART_PLUS));
	f_hbox1->Add (m_categoryadd_button, 0, wxLEFT, 5);

	m_categorydelete_button = new wxBitmapButton (f_panel, wxB_CATEGORY_DELETE, wxArtProvider::GetBitmap (wxART_MINUS));
	f_hbox1->Add (m_categorydelete_button, 0, wxLEFT, 5);

	m_categoryup_button = new wxBitmapButton (f_panel, wxB_CATEGORY_UP, wxArtProvider::GetBitmap (wxART_GO_UP));
	f_hbox1->Add (m_categoryup_button, 0, wxLEFT, 5);

	m_categorydown_button = new wxBitmapButton (f_panel, wxB_CATEGORY_DOWN, wxArtProvider::GetBitmap (wxART_GO_DOWN));
	f_hbox1->Add (m_categorydown_button, 0, wxLEFT, 5);

	/* second row */
	wxBoxSizer* f_hbox2 = new wxBoxSizer(wxHORIZONTAL);

	m_treectrl = new wxTreeCtrl (f_panel, wxB_TREECTRL, wxDefaultPosition, wxSize (200,200), wxTR_DEFAULT_STYLE|wxTR_EDIT_LABELS);
	m_rootid = m_treectrl->AddRoot ("Bookmarks");
	wxArrayString cat = marks->GetAllCategories ();
	for (int i = 0; i < cat.GetCount (); ++i)
	{
		m_treectrl->AppendItem (m_rootid, cat[i]);
	}
	m_treectrl->Expand (m_rootid);
	m_treectrl->ClearFocusedItem ();
	f_hbox2->Add (m_treectrl, 1, wxEXPAND|wxALL, 1);

	f_vbox1->Add (f_hbox1, 0, wxEXPAND, 5);
	f_vbox1->Add (f_hbox2, 1, wxEXPAND|wxALL, 5);
	f_panel->SetSizer (f_vbox1);
	
	/* Disable all buttons */
	m_categoryadd_button->Enable (false);
	m_categorydelete_button->Enable (false);
	m_categoryup_button->Enable (false);
	m_categorydown_button->Enable (false);

	/* Connect callbacks */
	Connect (wxB_TREECTRL, wxEVT_TREE_SEL_CHANGED, (wxObjectEventFunction) &WxbManageBookmarkDialog::OnTreeCtrlChanged);
	Connect (wxB_TREECTRL, wxEVT_TREE_ITEM_ACTIVATED, (wxObjectEventFunction) &WxbManageBookmarkDialog::OnTreeCtrlActivated);

	Connect (wxB_CATEGORY_ADD, wxEVT_BUTTON, (wxObjectEventFunction) &WxbManageBookmarkDialog::OnButtonCategoryAdd);
	Connect (wxB_CATEGORY_DELETE, wxEVT_BUTTON, (wxObjectEventFunction) &WxbManageBookmarkDialog::OnButtonCategoryDelete);
	Connect (wxB_CATEGORY_UP, wxEVT_BUTTON, (wxObjectEventFunction) &WxbManageBookmarkDialog::OnButtonCategoryUp);
	Connect (wxB_CATEGORY_DOWN, wxEVT_BUTTON, (wxObjectEventFunction) &WxbManageBookmarkDialog::OnButtonCategoryDown);

	Connect (wxB_LISTCTRL, wxEVT_LIST_ITEM_SELECTED, (wxObjectEventFunction) &WxbManageBookmarkDialog::OnListCtrlItemSelected);
	return f_panel;
}

wxPanel* WxbManageBookmarkDialog::CreateRightPanel (wxWindow* parent)
{
	wxPanel* f_panel = new wxPanel (parent, wxID_ANY);

	wxBoxSizer *f_vbox1 = new wxBoxSizer(wxVERTICAL);
	
	/* first row */
	wxBoxSizer* f_hbox1 = new wxBoxSizer (wxHORIZONTAL);

	m_bookmarkedit_button = new wxButton (f_panel, wxB_BOOKMARK_EDIT, "Edit bookmark");
	f_hbox1->Add (m_bookmarkedit_button, 0, wxLEFT, 5);

	m_bookmarkdelete_button = new wxBitmapButton (f_panel, wxB_BOOKMARK_DELETE, wxArtProvider::GetBitmap (wxART_DEL_BOOKMARK));
	f_hbox1->Add (m_bookmarkdelete_button, 0, wxLEFT, 5);

	/* second row */
	wxBoxSizer* f_hbox2 = new wxBoxSizer(wxHORIZONTAL);

	m_listctrl = new wxListCtrl(f_panel, wxB_LISTCTRL, wxDefaultPosition, wxDefaultSize, wxLC_REPORT|wxLC_SINGLE_SEL);

	m_listctrl->AppendColumn ("Name", wxLIST_FORMAT_CENTRE, wxLIST_AUTOSIZE_USEHEADER );
	m_listctrl->AppendColumn ("Address", wxLIST_FORMAT_CENTRE, wxLIST_AUTOSIZE_USEHEADER );
	m_listctrl->AppendColumn ("Description", wxLIST_FORMAT_CENTRE, wxLIST_AUTOSIZE_USEHEADER );
	f_hbox2->Add (m_listctrl, 1, wxEXPAND|wxALL, 1);

	f_vbox1->Add (f_hbox1, 0, wxEXPAND, 5);
	f_vbox1->Add (f_hbox2, 1, wxEXPAND|wxALL, 5);
	f_panel->SetSizer (f_vbox1);

	/* Disable all buttons */
	m_bookmarkedit_button->Enable (false);
	m_bookmarkdelete_button->Enable (false);

	/* Connect callbacks */
	Connect (wxB_BOOKMARK_EDIT, wxEVT_BUTTON, (wxObjectEventFunction) &WxbManageBookmarkDialog::OnButtonBookmarkEdit);
	Connect (wxB_BOOKMARK_DELETE, wxEVT_BUTTON, (wxObjectEventFunction) &WxbManageBookmarkDialog::OnButtonBookmarkDelete);

	return f_panel;
}

void WxbManageBookmarkDialog::UpdateUI (e_ui_state state)
{
	switch (state)
	{
	case STATE_ROOT:
		m_categoryadd_button->Enable (true);
		m_categorydelete_button->Enable (false);
		m_categoryup_button->Enable (false);
		m_categorydown_button->Enable (false);
	break;
	case STATE_NORMAL:
		m_categoryadd_button->Enable (true);
		m_categorydelete_button->Enable (true);
		m_categoryup_button->Enable (true);
		m_categorydown_button->Enable (true);
	break;
	case STATE_LISTITEM_SELECTED:
		m_bookmarkedit_button->Enable (true);
		m_bookmarkdelete_button->Enable (true);
	break;
	case STATE_LISTITEM_UNSELECTED:
		m_bookmarkedit_button->Enable (false);
		m_bookmarkdelete_button->Enable (false);
	break;
	};
}

/* single mouse click */
void WxbManageBookmarkDialog::OnTreeCtrlChanged (wxTreeEvent& event)
{
	if (event.GetItem () == m_rootid)
	{
		UpdateUI (STATE_ROOT);
	}
	else
	{
		UpdateUI (STATE_NORMAL);
	}
}

/* double mouse click */
void WxbManageBookmarkDialog::OnTreeCtrlActivated (wxTreeEvent& event)
{
	std::vector <struct s_FavoriteRow> f_favorites;
	
	s_FavoriteRow *f_fav_row;
	
	if (event.GetItem () == m_rootid)
	{
		UpdateUI (STATE_ROOT);
	}
	else
	{
		UpdateUI (STATE_NORMAL);
		f_favorites = marks->GetFavoritesByCategory (m_treectrl->GetItemText (event.GetItem ()));
		m_listctrl->DeleteAllItems ();
		UpdateUI (STATE_LISTITEM_UNSELECTED);
		for (int i= 0; i < f_favorites.size(); ++i)
		{
			AddRow (f_favorites[i].name, f_favorites[i].address, f_favorites[i].description);
		}
		
	}
}

void WxbManageBookmarkDialog::OnButtonCategoryAdd (wxCommandEvent& event)
{
	wxTextEntryDialog dialog (this, "Enter the category name");
	dialog.SetTextValidator (wxFILTER_EMPTY);

	if (dialog.ShowModal () == wxID_OK)
	{
		m_treectrl->AppendItem (m_rootid, dialog.GetValue());
		try
		{
			marks->SaveCategory (dialog.GetValue());
		}
		catch (std::exception &e)
		{
			wxLogMessage (wxString (e.what (), wxConvUTF8), _("Exception caught"));
		}
		// m_treectrl->AppendItem (m_treectrl->GetFocusedItem (), dialog.GetValue());
		m_treectrl->Expand (m_rootid);
	}
}

void WxbManageBookmarkDialog::OnButtonCategoryDelete (wxCommandEvent& event)
{
	wxTreeItemId item;
	wxString text;

	item = m_treectrl->GetFocusedItem ();
	try
	{
		marks->DeleteCategory (m_treectrl->GetItemText (item));
	}
	catch (std::exception &e)
	{
		wxLogMessage (wxString (e.what (), wxConvUTF8), _("Exception caught"));
	}
	m_treectrl->Delete (item);
}

void WxbManageBookmarkDialog::OnButtonCategoryUp (wxCommandEvent& event)
{
	
}

void WxbManageBookmarkDialog::OnButtonCategoryDown (wxCommandEvent& event)
{
	
}

void WxbManageBookmarkDialog::OnButtonBookmarkEdit (wxCommandEvent& event)
{
}

void WxbManageBookmarkDialog::OnButtonBookmarkDelete (wxCommandEvent& event)
{
	WxbChangeCategoryDialog* dialog = new WxbChangeCategoryDialog (this, _("Change category"));
		if (dialog->ShowModal () == wxID_OK)
		{
		}
		dialog->Destroy ();
		delete dialog;
}

void WxbManageBookmarkDialog::OnListCtrlItemSelected (wxListEvent& event)
{
	UpdateUI (STATE_LISTITEM_SELECTED);
}

void WxbManageBookmarkDialog::AddRow (wxString Name, wxString Address, wxString Description)
{
	long index;

	index = m_listctrl->InsertItem (m_listctrl->GetItemCount (), Name);
	m_listctrl->SetItem (index, 1, Address);
	m_listctrl->SetItem (index, 2, Description);
}
