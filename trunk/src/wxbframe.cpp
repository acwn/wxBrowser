/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <wx/artprov.h>
#include <wx/aboutdlg.h>

#include "wxbframe.h"
#include "wxbpreferences.h"
#include "wxbbookmarks.h"
#include "wxbmanagebookmarkdialog.h"
#include "wxbevents.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/resource.h>



WxbFrame::WxbFrame (wxLocale& locale, const wxString& title, const wxPoint& pos, const wxSize& size) : wxFrame(NULL, -1, title, pos, size), m_locale(locale)
{
	// core dumps may be disallowed by parent of this process; change that
	struct rlimit core_limits;
	core_limits.rlim_cur = core_limits.rlim_max = RLIM_INFINITY;
	setrlimit(RLIMIT_CORE, &core_limits);

	m_Options = new WxbOptions ();
	if (m_Options->FetchIntSetting ("/General/savesize"))
	{
		SetSize (m_Options->FetchIntSetting ("/General/width"), m_Options->FetchIntSetting ("/General/height"));
	}
	CreateStatusBar (2);
	SetStatusText ( _("Welcome to ") + PACKAGE_NAME);

	/* Load program icon */
	wxString iconfile (DATADIR);
	iconfile.Append ("/icons/32x32/internet-web-browser.png");
	wxIcon icon (iconfile);
	SetIcon (icon);

	wxToolBar *toolbar = CreateToolBar ();

	/* Quit program */
	wxBitmap exit = wxArtProvider::GetBitmap (wxART_QUIT);
	toolbar->InsertTool (0, wxID_EXIT, _("Exit application"), exit, wxNullBitmap, wxITEM_NORMAL, _("Quit wxBrowser"), _("Quit wxBrowser"));
	toolbar->AddSeparator ();

	/* New tab */
	iconfile.clear ();
	iconfile.Append (DATADIR);
	iconfile.Append ("/icons/22x22/tab-new.png");
	wxBitmap newtab (iconfile);
	toolbar->InsertTool (2, wxID_NEW, _("New tab"), newtab, wxNullBitmap, wxITEM_NORMAL, _("New tab"), _("New tab"));

	/* Close tab */
	wxBitmap closetab = wxArtProvider::GetBitmap (wxART_CLOSE);
	toolbar->InsertTool (3, wxID_CLOSE, _("Close tab"), closetab, wxNullBitmap, wxITEM_NORMAL, _("Close tab"), _("Close tab"));

	/* Show bookmarks */
	wxBitmap bookmarks = wxArtProvider::GetBitmap (wxART_REPORT_VIEW);
	toolbar->InsertTool (4, wxB_BOOKMARKS, _("Bookmarks"), bookmarks, wxNullBitmap, wxITEM_NORMAL, _("Bookmarks"), _("Bookmarks"));
	toolbar->AddSeparator ();

	wxBitmap preferences = wxArtProvider::GetBitmap (wxART_HELP_SETTINGS);
	toolbar->InsertTool (6, wxID_PREFERENCES, _("Preferences"), preferences, wxNullBitmap, wxITEM_NORMAL, _("Preferences"), _("Preferences"));

	wxBitmap about = wxArtProvider::GetBitmap (wxART_HELP);
	toolbar->InsertTool (7, wxID_ABOUT, _("Help"), about, wxNullBitmap, wxITEM_NORMAL, _("About"), _("About"));

	toolbar->Realize ();

	/* layout windows */
	wxPanel *panel = new wxPanel (this, -1);
	wxBoxSizer *vsizer = new wxBoxSizer (wxVERTICAL);
	tabbook = new WxbTabbook (panel, wxB_TABBOOK, m_Options->FetchStringSetting ("/General/startpage"));
	vsizer->Add (tabbook, 1, wxEXPAND|wxALL, 0);
	panel->SetSizer (vsizer);

	/* Callbacks */
	Connect (wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, (wxObjectEventFunction) &WxbFrame::OnQuit);
	Connect (wxID_NEW, wxEVT_COMMAND_MENU_SELECTED, (wxObjectEventFunction) &WxbFrame::OnNewTab);
	Connect (wxID_CLOSE, wxEVT_COMMAND_MENU_SELECTED, (wxObjectEventFunction) &WxbFrame::OnCloseTab);
	Connect (wxB_BOOKMARKS, wxEVT_COMMAND_MENU_SELECTED, (wxObjectEventFunction) &WxbFrame::OnBookmarks);
	Connect (wxID_PREFERENCES, wxEVT_COMMAND_MENU_SELECTED, (wxObjectEventFunction) &WxbFrame::OnPreferences);
	Connect (wxID_ABOUT, wxEVT_COMMAND_MENU_SELECTED, (wxObjectEventFunction) &WxbFrame::OnAbout);
	Connect (-1, WXBEVT_SETMAINTITLE, (wxObjectEventFunction) &WxbFrame::OnSetMainTitle);
	Connect (-1, wxEVT_CHAR_HOOK, (wxObjectEventFunction) &WxbFrame::OnKeyDown);
}

WxbFrame::~WxbFrame ()
{
	delete m_Options;
}

void WxbFrame::OnQuit (wxCommandEvent& event)
{
	Close(true);
}

void WxbFrame::OnNewTab (wxCommandEvent& event)
{
	tabbook->AddTab (m_Options->FetchStringSetting ("/General/startpage"));
}

void WxbFrame::OnCloseTab (wxCommandEvent& event)
{
	tabbook->CloseTab ();
}

void WxbFrame::OnPopupClick (wxCommandEvent &event)
{
	int id = event.GetId ();
	if (id == 200)
	{
		WxbManageBookmarkDialog* dialog = new WxbManageBookmarkDialog (this, _("Manage bookmark"));
		if (dialog->ShowModal () == wxID_OK)
		{
		}
		dialog->Destroy ();
		delete dialog;
	}
	else
	{
		WxbBookmarks bookmarks;
		tabbook->AddTab (bookmarks.GetAddress (event.GetId ()));
	}
}

// http://stackoverflow.com/questions/12327098/c-dynamic-wx-menu-bindings
void WxbFrame::OnBookmarks (wxCommandEvent& event)
{
	wxArrayString f_submenunames;
	std::vector <struct s_FavoriteRow> f_favorites;
	WxbBookmarks bookmarks;
	wxMenu* menu;
	wxPoint posi;
	wxToolBar *object = (wxToolBar*) event.GetEventObject ();

	posi = object->GetPosition () + wxPoint (100,0);
	try
	{
		// menu = bookmarks.GetMenu ();
		
		menu = new wxMenu();
		menu->Append (200, "Manage bookmarks");
		menu->AppendSeparator();
		
		f_submenunames = bookmarks.GetAllCategories ();
		wxMenu *submenus[f_submenunames.GetCount()];

		for (int i=0; i < f_submenunames.GetCount(); ++i)
		{
			submenus[i] = new wxMenu ();
			menu->AppendSubMenu (submenus[i], f_submenunames[i]);
			f_favorites = bookmarks.GetFavoritesByCategory (f_submenunames[i]);
			for (int j= 0; j < f_favorites.size(); ++j)
			{
				submenus[i]->Append (f_favorites[j].id, f_favorites[j].name, f_favorites[j].address);
				submenus[i]->Connect (wxEVT_COMMAND_MENU_SELECTED, (wxObjectEventFunction)&WxbFrame::OnPopupClick, NULL, this);
			}
		}
	
		
	}
	catch (const std::exception& e)
	{
		wxLogMessage (wxString (e.what (), wxConvUTF8), _("Exception caught"));
	}
	menu->Connect (wxEVT_COMMAND_MENU_SELECTED, (wxObjectEventFunction)&WxbFrame::OnPopupClick, NULL, this);
	PopupMenu (menu, posi);
}

void WxbFrame::OnPreferences (wxCommandEvent& event)
{
	WxbPreferences* pref = new WxbPreferences (this, _("Preferences"));
	pref->SetSettings (m_Options->GetSettings ());
	if (pref->ShowModal () == wxID_OK)
	{
		m_Options->SetSettings (pref->GetSettings ());
		tabbook->UpdateStartpage (m_Options->FetchStringSetting ("/General/startpage"));
	}
	pref->Destroy ();
}

void WxbFrame::OnAbout (wxCommandEvent& event)
{
	wxAboutDialogInfo about;
	about.SetName (PACKAGE_NAME);
	about.SetVersion (PACKAGE_VERSION);
	about.SetDescription (_("A simple web browser without faffing around"));
	about.SetCopyright ("Copyright (c) 2011 - 2016 Alexander Nagel");
	about.SetWebSite (PACKAGE_URL);
	about.AddDeveloper ("me");
	wxString iconfile (DATADIR);
	iconfile.Append ("/icons/32x32/internet-web-browser.png");
	wxIcon icon (iconfile);
	about.SetIcon (icon);	
	wxAboutBox (about);
}

void WxbFrame::OnSetMainTitle (wxCommandEvent& event)
{
	wxString f_text = event.GetString ();
	f_text.Append (" - ");
	f_text.Append (PACKAGE_NAME);
	f_text.Append (" ");
	f_text.Append (PACKAGE_VERSION);

	SetTitle (f_text);
}

void WxbFrame::OnKeyDown (wxKeyEvent& event)
{
	switch (event.GetModifiers ())
	{
		case wxMOD_CONTROL:
			switch (event.GetKeyCode ())
			{
				case wxKeyCode ('T'):
					tabbook->AddTab (m_Options->FetchStringSetting ("/General/startpage"));
				break;
				case wxKeyCode ('W'):
					tabbook->CloseTab ();
					event.Skip (false);
					return;
				break;
			}
		break;
	}
	event.Skip ();
}
