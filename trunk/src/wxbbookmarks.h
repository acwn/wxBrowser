/*

This file is part of wxBrowser.

wxBrowser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

wxBrowser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with wxBrowser.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __WXBBOOKMARKS__
#define __WXBBOOKMARKS__

#include <string>
#include <vector>

#include <sqlite3.h>

#include <wx/menu.h>
#include <wx/dir.h>


struct s_CategoryRow
{
	int id;
	wxString name;
	wxString description;
	int order;
	int parent;
};

struct s_FavoriteRow
{
	int id;
	wxString name;
	wxString address;
	wxString description;
	int cat_id;
};

class WxbBookmarks
{
	private:
		/* SQLITE */
		wxString m_file;
		sqlite3 *m_dbconnection;
		bool Execute (wxString query);
		bool Execute (wxString query, std::vector <wxString> m_CategoryList);

		/**
		 * @brief Callback function called by Execute
		 * @param pointer   This is a reference to the fourth argument of sqlite3_exec.
		 * @param argc      The number of columns in the result
		 * @param argv      An array of pointers to strings obtained as if from sqlite3_column_text(), one for each column.
		 * @param azColName An array of pointers to strings where each entry represents the name of corresponding result column as obtained from sqlite3_column_name().
		 * @return          It normally returns zero (0).
		 */
		static int Callback (void *pointer, int argc, char **argv, char **azColName);

		std::vector <wxString> m_CategoryList;
		std::vector <struct s_CategoryRow> m_Categories;
		std::vector <struct s_FavoriteRow> m_Favorites;

	public:
		WxbBookmarks (void);
		~WxbBookmarks ();
		wxMenu* GetMenu (void);

		/* Favorites */
		void SaveFavorite (wxString name, wxString address, wxString category, wxString description);
		void DeleteFavorite (wxString name);
		std::vector <wxArrayString> GetAllFavorites (wxString category);
		std::vector <struct s_FavoriteRow> GetFavoritesByCategory (wxString category);
		wxString GetAddress (int ID); 

		/* Catogories */
		void SaveCategory (wxString category, wxString description = wxEmptyString, int sequence = 0);
		void DeleteCategory (wxString category);
		wxArrayString GetAllCategories (void);
		int GetCategoryID (wxString category);
};

#endif
